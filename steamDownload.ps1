#Downloads New TERA update from steam if TERA.exe has changed
#Launches Binary
#Runs Dump Script

#load config
$config = Get-Content .\config.json -Raw | ConvertFrom-Json
Write-Host "Loaded config! Downloading binaries file..."

#Depot Downloader
.\deps\depotdownloader-2.4.5\DepotDownloader.exe `
-app $config.steamGame.app `
-depot $config.steamGame.depot `
-username $config.steamAccount.username `
-password $config.steamAccount.password `
-filelist file.txt
Write-Host "Done!"

#pull changes from upstream
Set-Location .\tera-client-dump
git pull
Set-Location ..
Write-Host "Pulled tatest changes from dump repo"

#Move Release Revision to repo
Copy-Item .\depots\$($config.steamGame.depot)\*\Client\Binaries\ReleaseRevision.txt .\tera-client-dump\

#Parse version from Release Revision
$clientVersion = (Get-ChildItem .\tera-client-dump\ReleaseRevision.txt | `
Select-String -Pattern "(?<=Version: ).*").Matches

#Start TERA
Start-Process .\depots\$($config.steamGame.depot)\*\Client\Binaries\TERA.exe
Write-Host "Started TERA client. Waiting 10 seconds then starting dump."
Start-Sleep 10 #idk how to detect the actual error window

#Scylla Headless Dump
.\deps\ScyllaHeadless_v1.0.0_x64\ScyllaHeadless.exe "TERA.exe" "$(Get-Location)\tera-client-dump\TERA_dump.exe"
Write-Host "Dump Suceeded! Closing Client and commiting files."
Stop-Process -Name "TERA"

#Goto tera-client-dump
Set-Location .\tera-client-dump
git add .
git commit -m "Updated to $clientVersion"
git push

#cleanup
Set-Location ..
Remove-Item ./depots -Recurse -Force

Write-Host "Update Commited Sucessfully!"

Start-Sleep 30 #gives you a chance to stop the script before the machine shutsdown
Stop-Computer -ComputerName arborean

